import { Angular4V.1Page } from './app.po';

describe('angular4-v.1 App', () => {
  let page: Angular4V.1Page;

  beforeEach(() => {
    page = new Angular4V.1Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
